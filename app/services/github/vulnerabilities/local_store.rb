# frozen_string_literal: true

module Github
  module Vulnerabilities
    # Local store for package ecosystem vulnerabilities
    #
    class LocalStore < ApplicationService
      def initialize(package_ecosystem)
        @package_ecosystem = package_ecosystem
      end

      def call
        RequestStore.fetch(:"#{package_ecosystem}-vulnerabilities") { fetch_vulnerabilities }
      end

      private

      attr_reader :package_ecosystem

      # Active vulnerabilities
      #
      # @return [Array<Vulnerability>]
      def fetch_vulnerabilities
        unless Fetcher::PACKAGE_ECOSYSTEMS.key?(package_ecosystem)
          log(:info, "Ecosystem #{package_ecosystem} not supported")
          return []
        end

        if CredentialsConfig.github_access_token.blank?
          log(:warn, "Missing GitHub access token. Security vulnerabilities will not be detected.")
          return []
        end

        Github::Vulnerabilities::Fetcher.call(package_ecosystem).select do |vulnerability|
          vulnerability.withdrawn_at.nil?
        end
      rescue StandardError => e
        log_error(e, message_prefix: "Failed to fetch vulnerabilities. Security vulnerabilities will not be detected")
        []
      end
    end
  end
end
