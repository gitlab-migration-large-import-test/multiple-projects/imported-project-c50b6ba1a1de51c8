# frozen_string_literal: true

module Dependabot
  module ServiceMode
    module UpdateService
      include ServiceHelpersConcern

      # Initialize gitlab client and sync config and run update
      #
      # @return [void]
      def call
        init_gitlab(project)
        sync_config

        super
      end

      # Fetch up to date config if app is not integrated
      #
      # @return [void]
      def sync_config
        return if AppConfig.integrated?

        project.configuration = fetch_config
      end

      # Persisted project
      #
      # @return [Project]
      def project
        @project ||= Project.find_by(name: project_name)
      end

      # Close obsolete vulnerability alerts
      #
      # @param [Dependabot::Dependencies::UpdatedDependency] dependency
      # @return [void]
      def close_obsolete_vulnerability_issues(dependency)
        vulnerability_issues = project.open_vulnerability_issues(
          package_ecosystem: package_ecosystem,
          directory: directory,
          package: dependency.name
        )
        obsolete_issues = vulnerability_issues.reject { |issue| issue.vulnerability.vulnerable?(dependency.version) }
        return if obsolete_issues.empty?

        log(:info, "  closing obsolete vulnerability issues")
        obsolete_issues.each do |issue|
          log(:debug, "   closing obsolete issue !#{issue.iid} because dependency version is not vulnerable")
          Gitlab::Vulnerabilities::IssueCloser.call(issue)
        rescue Gitlab::Error::ResponseError => e
          log_error(e, message_prefix: "   failed to close vulnerability issue")
        end
      end

      # Close obsolete merge requests
      #
      # @param [String] dependency_name
      # @return [void]
      def close_obsolete_mrs(dependency_name)
        return if DependabotConfig.dry_run?

        obsolete_mrs = project.open_dependency_merge_requests(dependency_name, directory)
        return if obsolete_mrs.empty?

        log(:info, "  closing obsolete merge requests")
        obsolete_mrs.each { |mr| close_mr(mr) }
      end

      # Close merge request
      #
      # @param [MergeRequest] merge_request
      # @return [void]
      def close_mr(merge_request)
        iid = merge_request.iid
        log(:debug, "Closing obsolete merge request !#{iid} because dependency version is up to date")
        return unless mr_state_in_sync?(merge_request)

        Gitlab::BranchRemover.call(project_name, merge_request.branch)
        merge_request.close

        Gitlab::MergeRequest::Commenter.call(
          project_name,
          iid,
          "This merge request has been closed because the dependency version is up to date."
        )
      rescue Gitlab::Error::Error => e
        log_error(e, message_prefix: "Failed to close obsolete merge request: !#{iid}")
      end

      # Check if merge request state is in sync with GitLab
      #
      # @param [MergeRequest] merge_request
      # @return [Boolean]
      def mr_state_in_sync?(merge_request)
        iid = merge_request.iid
        mr = gitlab.merge_request(project_name, iid)

        if mr.state != "opened"
          # This should not happen if webhooks are set up
          log(AppConfig.integrated? ? :warn : :debug, "  merge request !#{iid} state is not in sync with GitLab")
          merge_request.close
          return false
        end

        true
      end
    end
  end
end
