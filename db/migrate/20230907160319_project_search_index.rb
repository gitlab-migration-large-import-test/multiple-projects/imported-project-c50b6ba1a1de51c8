# frozen_string_literal: true

class ProjectSearchIndex < Mongoid::Migration
  def self.up
    Project.create_indexes
  end

  def self.down; end
end
